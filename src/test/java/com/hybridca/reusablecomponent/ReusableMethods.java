package com.hybridca.reusablecomponent;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.hybridca.utility.PropertyFileReader;

public class ReusableMethods {
	
	public static void loadurl(WebDriver driver) {
		driver.get(PropertyFileReader.loadfile().getProperty("url"));

	}
	
	public static void timelapse(WebDriver driver) {
		int time = Integer.parseInt(PropertyFileReader.loadfile().getProperty("wait10"));
		driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
	}
	
	public static boolean getElement(By selector, WebDriver driver) {
		try {
			driver.findElement(selector);
			return true;
		} catch (Exception e) {
			System.out.println(String.format("Element %s does not Exist - proceeding", selector));
		}
		return false;
	}
	
	public static boolean getElement(By selector, WebElement element) {
		try {
			element.findElement(selector);
			return true;
		} catch (Exception e) {
			System.out.println(String.format("Element %s does not Exist - proceeding", selector));
		}
		return false;
	}
	
	public static boolean sendKeys(By selector, String value, WebDriver driver) {
		try {
			driver.findElement(selector).sendKeys(value);
			return true;
		} catch (Exception e) {
			System.out.println(String.format("Error Sending %s to Element %s", value, selector));
		}
		return false;
	}
	
public static boolean click(By selector, WebDriver driver) {
		try {
			driver.findElement(selector).click();
			return true;
		} catch (Exception e) {
			System.out.println(String.format("Error clicking Element %s", selector));
		}
		return false;
	}

	public static boolean switchTab(WebDriver driver) {
		try {
			ArrayList<String> windowHandles = new ArrayList<String> (driver.getWindowHandles());
			String currenthandle = driver.getWindowHandle();
			int index = windowHandles.indexOf(currenthandle);
		    driver.switchTo().window(windowHandles.get(++index));
		    return true;
		} catch (Exception e) {
			System.out.println("No Tab(s) Found!");
		}
		return false;
	}
	
}
