package com.hybridca.reusablecomponent;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.hybridca.utility.PropertyFileReader;

public class ReusableComponent {
	static WebDriver driver;

	public static WebDriver initializeDriver() {
		String browserName = PropertyFileReader.loadfile().getProperty("browser");

		if (browserName.equalsIgnoreCase("chrome")) {
			
			System.setProperty("webdriver.chrome.driver", PropertyFileReader.loadfile().getProperty("chromedriver"));
			driver = new ChromeDriver();
			
		} else if(browserName.equalsIgnoreCase("firefox")) {
			
			System.setProperty("webdriver.gecko.driver", PropertyFileReader.loadfile().getProperty("firefoxdriver"));
			driver = new FirefoxDriver();
			
		} else if(browserName.equalsIgnoreCase("edge")) {
			
			System.setProperty("webdriver.edge.driver", PropertyFileReader.loadfile().getProperty("edgedriver"));
			driver = new EdgeDriver();
			
		}
		
		driver.manage().window().maximize();
		ReusableMethods.timelapse(driver);
		
		return driver;
	}
}
