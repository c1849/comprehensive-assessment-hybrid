package com.hybridca.pageobject;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.hybridca.utility.ExtentReport;
import com.hybridca.utility.Log;
import com.relevantcodes.extentreports.ExtentTest;

public class CategoryPage {
	
	public static boolean openOption(WebDriver driver, int index) {
		
		try {
			List<WebElement> taxoncards = driver.findElements(By.className("taxon-card"));
			WebElement opt1 = taxoncards.get(index);
			
			opt1.click();
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public static boolean categoryOpen(WebDriver driver, Logger log, ExtentTest test) {
//		List<WebElement> taxoncards = driver.findElements(By.className("taxon-card"));
//		WebElement opt1 = taxoncards.get(0);
//		
//		opt1.click();
		
		if (openOption(driver, 0)) {
			Log.logInfo(log, "Category Found and Clicked");
			ExtentReport.LogInfo(test, "Category Found and Clicked");
			return true;
		}
		return false;
	}
}
