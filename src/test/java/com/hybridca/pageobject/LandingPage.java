package com.hybridca.pageobject;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.hybridca.reusablecomponent.ReusableMethods;
import com.hybridca.uistore.LandingPageUI;
import com.hybridca.utility.ExtentReport;
import com.hybridca.utility.Log;
import com.relevantcodes.extentreports.ExtentTest;

public class LandingPage {

	public static List<WebElement> topnavbar(WebDriver driver) {
		List<WebElement> nav_items = new ArrayList<WebElement>();
		try {
			nav_items = driver.findElements(By.xpath("//div[@id='topnav_wrapper']/*/li"));
		} catch (Exception e) {
			System.out.println("Element does not Exist - proceeding");
		}
		return nav_items;
	}

	public static WebElement topnavcategory(WebDriver driver, int index) {
		List<WebElement> nav_items = topnavbar(driver);
		WebElement category = null;
		try {
			category = nav_items.get(index);
		} catch (Exception e) {
			System.out.println("Element does not Exist - proceeding");
		}
		return category;
	}

	public static WebElement topnavsubcategory(WebElement category, int sublistIndex, int subitemIndex) {
		WebElement category_subitem = null;
		try {
			List<WebElement> category_sublists = category.findElements(By.xpath(".//li[@class='sublist_item']"));
			List<WebElement> category_subitems = category_sublists.get(sublistIndex)
					.findElements(By.cssSelector(".subnav_item"));
			category_subitem = category_subitems.get(subitemIndex);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return category_subitem;
	}

	public static boolean topnavOpen(WebDriver driver, Logger log, ExtentTest test) {
		WebElement category = topnavcategory(driver, 0);
		if (category != null) {
			category.click();
			Log.logInfo(log, "Navigation Bar Categories Found and Clicked");
			ExtentReport.LogInfo(test, "Navigation Bar Categories Found and Clicked");
			WebElement category_subitem = topnavsubcategory(category, 0, 0);
			if (category_subitem != null) {
				category_subitem.click();
				Log.logInfo(log, "Navigation Bar Sub Category Found and Clicked");
				ExtentReport.LogInfo(test, "Navigation Bar Sub Category Found and Clicked");
				return true;
			}
		}
		return false;
	}

	public static WebElement footerRow(WebDriver driver, int rowIndex) {
		WebElement footerRow = null;
		try {
			WebElement footer = driver.findElement(By.cssSelector(".footer-delivery"));
			List<WebElement> footerRows = footer.findElements(By.cssSelector(".row"));
			footerRow = footerRows.get(rowIndex);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return footerRow;
	}

	public static WebElement rowItems(WebElement footerRow, int itemIndex) {
		WebElement rowItem = null;
		try {
			List<WebElement> categories = footerRow.findElements(By.className("category"));
			rowItem = categories.get(0);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return rowItem;
	}

	public static boolean footerOpen(WebDriver driver, Logger log, ExtentTest test) {
//		WebElement footer = driver.findElement(By.cssSelector(".footer-delivery"));
//		List<WebElement> footerRows = footer.findElements(By.cssSelector(".row"));
//		WebElement popFurniture = footerRows.get(0);
		WebElement footerRow = footerRow(driver, 0);
		if (footerRow != null) {
//			List<WebElement> categories = footerRow.findElements(By.className("category"));
			Log.logInfo(log, "Footer Categories Found");
			ExtentReport.LogInfo(test, "Footer Categories Found");
			WebElement rowItem = rowItems(footerRow, 0);
			if (rowItem != null) {
				rowItem.click();
				Log.logInfo(log, "Footer subItem Found and Clicked");
				ExtentReport.LogInfo(test, "Footer subItem Found and Clicked");
				return true;
			}
		}
		return false;
	}

	public static WebElement landingExplore(WebDriver driver, int index) {
		WebElement category = null;
		try {
			List<WebElement> categories = driver.findElements(By.xpath("//*[contains(@class,'categories')]//*[@class='category']"));
			category = categories.get(index);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return category;
	}
	
	public static boolean landingpageOpen(WebDriver driver, Logger log, ExtentTest test) {
//		List<WebElement> categories = driver.findElements(By.xpath("//*[contains(@class,'categories')]//*[@class='category']"));
//		WebElement sofas = categories.get(0);
		WebElement category = landingExplore(driver, 1);
		if (category != null) {
			category.click();
			Log.logInfo(log, "Landing Page Category Found and Clicked");
			ExtentReport.LogInfo(test, "Landing Page Category Found and Clicked");
			return true;
		}
		return false;
	}

	public static boolean search(WebDriver driver, String searchItem, Logger log, ExtentTest test) {
		if (ReusableMethods.getElement(LandingPageUI.searchbox, driver)) {
			Log.logInfo(log, "Search Box found");
			ExtentReport.LogInfo(test, "Search Box found");
			if (ReusableMethods.sendKeys(LandingPageUI.searchbox, searchItem, driver)) {
				Log.logInfo(log, "Input Done");
				ExtentReport.LogInfo(test, "Input Done");
				if (ReusableMethods.click(LandingPageUI.searchbutton, driver)) {
					Log.logInfo(log, "Search Button clicked");
					ExtentReport.LogInfo(test, "Search Button clicked");
					return true;
				}
			}
		}
		return false;
	}
}
