package com.hybridca.pageobject;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.hybridca.utility.ExtentReport;
import com.hybridca.utility.Log;
import com.relevantcodes.extentreports.ExtentTest;

public class ProductListPage {

	public static WebElement product(WebDriver driver, int index) {
		WebElement product = null;
		try {
			List<WebElement> productlist = driver.findElements(By.cssSelector("ul[class*='productlist'] > li"));
			product = productlist.get(index);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return product;
	}

	public static WebElement openProd(WebElement product) {
		WebElement open = null;
		try {
			open = product.findElement(By.xpath(".//a/div"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return open;
	}

	public static boolean catalogueOpen(WebDriver driver, Logger log, ExtentTest test) {
//		List<WebElement> productlist = driver.findElements(By.cssSelector("ul[class*='productlist'] > li"));
//		WebElement prod1 = productlist.get(0);
//		System.out.println(prod1.findElement(By.cssSelector("div[itemprop='name']")).getText().split("By Urban Ladder")[0]);
		WebElement product = product(driver, 0);
		if (product != null) {
			WebElement productlink = openProd(product);
			if (productlink != null) {
				productlink.click();
				Log.logInfo(log, "Product Found and Clicked");
				ExtentReport.LogInfo(test, "Product Found and Clicked");
				return true;
			}
		}
		return false;
	}
}
