package com.hybridca.runner;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.hybridca.pageobject.CategoryPage;
import com.hybridca.pageobject.LandingPage;
import com.hybridca.pageobject.ProductListPage;
import com.hybridca.pageobject.ProductPage;
//import com.hybridca.pageobject.HomePage;
import com.hybridca.reusablecomponent.ReusableComponent;
import com.hybridca.reusablecomponent.ReusableMethods;
import com.hybridca.utility.ExcelReader;
import com.hybridca.utility.ExtentReport;
import com.hybridca.utility.Log;
import com.hybridca.utility.SendMail;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class Runner {

	private ExtentReports report = ExtentReport.generateReport();
	private ExtentTest test;
	private Logger log = Log.logger(Runner.class.getName());
	WebDriver driver;

	@BeforeClass
	public void initalizelog() {
		Log.logfunction(log);
	}

	@BeforeMethod
	public void loaddriver() {
		driver = ReusableComponent.initializeDriver();
	}

	@BeforeTest
	@DataProvider(name = "searchData")
	public Object[][] searchData() {
		ArrayList<String> exceldata = ExcelReader.readExcel("search");
		Object[][] data = new Object[exceldata.size()][1];
		for (int i = 0; i < exceldata.size(); i++) {
			data[i][0] = exceldata.get(i);
		}
		return data;
	}

	@Test(priority = 0)
	public void loadsite() {
		test = report.startTest("Site Load");
		ReusableMethods.loadurl(driver);
		ExtentReport.ExtentPass(test, "Pass");
	}

	@Test(priority = 1)
	public void navbarProduct() {
		test = report.startTest("Product navigation through navbar");
		ReusableMethods.loadurl(driver);
		Log.logInfo(log, "Site Loaded");
		ExtentReport.LogInfo(test, "Site Loaded");
		boolean result = LandingPage.topnavOpen(driver, log, test) && ProductListPage.catalogueOpen(driver, log, test)
				&& ReusableMethods.switchTab(driver);

		try {
			System.out.println(ProductPage.getProductTitle(driver, "", log, test));
			Assert.assertTrue(result);
			Log.logInfo(log, "Page Opened Successful");
			ExtentReport.ExtentPass(test, "Page Opened Successful");
		} catch (Exception e) {
			Log.logError(log, "Page Opened Failed");
			ExtentReport.ExtentFail(test, ExtentReport.addScreenshot(test, driver));
		}

	}

	@Test(priority = 2)
	public void footerProduct() {
		test = report.startTest("Product navigation through footer");
		ReusableMethods.loadurl(driver);
		Log.logInfo(log, "Site Loaded");
		ExtentReport.LogInfo(test, "Site Loaded");

		boolean result = LandingPage.footerOpen(driver, log, test) && ReusableMethods.switchTab(driver)
				&& CategoryPage.categoryOpen(driver, log, test) && ReusableMethods.switchTab(driver)
				&& ProductListPage.catalogueOpen(driver, log, test) && ReusableMethods.switchTab(driver);
		try {
			System.out.println(ProductPage.getProductTitle(driver, "", log, test));
			Assert.assertTrue(result);
			Log.logInfo(log, "Page Opening Successful");
			ExtentReport.ExtentPass(test, "Page Opening Successful");
		} catch (Exception e) {
			Log.logError(log, "Page Opening Failed");
			ExtentReport.ExtentFail(test, ExtentReport.addScreenshot(test, driver));
		}
	}

	@Test(priority = 3, dataProvider = "searchData")
	public void searchboxProduct(String searchItem) {
		test = report.startTest("Product navigation through search box");
		ReusableMethods.loadurl(driver);
		Log.logInfo(log, "Site Loaded");
		ExtentReport.LogInfo(test, "Site Loaded");
		Boolean result = LandingPage.search(driver, searchItem, log, test) &&
		ProductListPage.catalogueOpen(driver, log, test) &&
		ReusableMethods.switchTab(driver);
		try {
			System.out.println(ProductPage.getProductTitle(driver, "", log, test));
			Assert.assertTrue(result);
			Log.logInfo(log, "Page Opening Successful");
			ExtentReport.ExtentPass(test, "Page Opening Successful");
		} catch (Exception e) {
			Log.logError(log, "Page Opening Failed");
			ExtentReport.ExtentFail(test, ExtentReport.addScreenshot(test, driver));
		}
	}

	@Test(priority = 4)
	public void landingProduct() {
		test = report.startTest("Product navigation through landing page");
		ReusableMethods.loadurl(driver);
		Log.logInfo(log, "Site Loaded");
		ExtentReport.LogInfo(test, "Site Loaded");
		boolean result = LandingPage.landingpageOpen(driver, log, test) && CategoryPage.categoryOpen(driver, log, test)
				&& ReusableMethods.switchTab(driver) && ProductListPage.catalogueOpen(driver, log, test)
				&& ReusableMethods.switchTab(driver);

		try {
			System.out.println(ProductPage.getProductTitle(driver, "", log, test));
			Assert.assertTrue(result);
			Log.logInfo(log, "Page Opening Successful");
			ExtentReport.ExtentPass(test, "Page Opening Successful");
		} catch (Exception e) {
			Log.logError(log, "Page Opening Failed");
			ExtentReport.ExtentFail(test, ExtentReport.addScreenshot(test, driver));
		}
	}

	@AfterMethod
	public void closedriver() {
		report.endTest(test);
		driver.quit();
	}

	@AfterClass
	public void quitdriver() {
		report.flush();
		driver.quit();
//		SendMail.sendMail();
	}
}
