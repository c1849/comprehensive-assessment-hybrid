package com.hybridca.uistore;

import org.openqa.selenium.By;

public class LandingPageUI {
	public static final By topnav_wrapper = By.id("topnav_wrapper");
	public static final By searchbox = By.id("search");
	public static final By searchbutton = By.id("search_button");
}
