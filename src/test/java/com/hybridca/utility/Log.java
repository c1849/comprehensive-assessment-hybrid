package com.hybridca.utility;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Log {

	public static Logger logger(String string) {
		return Logger.getLogger(string);
	}

	public static void logInfo(Logger log, String string) {
		log.info(string);	
	}

	public static void logDebug(Logger log, String string) {
		log.debug(string);	
	}
	
	public static void logWarn(Logger log, String string) {
		log.warn(string);	
	}
	
	public static void logError(Logger log, String string) {
		log.error(string);	
	}
	
	public static void logFatal(Logger log, String string) {
		log.fatal(string);	
	}

	public static void logfunction(Logger log) {
		PropertyConfigurator.configure("log4j.properties");
	}
}
