package com.hybridca.utility;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ExtentReport {

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd.HHmmss");
	
	public static String reportName = "Report"+ LocalDateTime.now().format(FORMATTER) +".html";

	public static ExtentReports generateReport() {
		return new ExtentReports(PropertyFileReader.loadfile().getProperty("report") + "ExtentReport.html");
	}

	public static void ExtentPass(ExtentTest test, String message) {
		test.log(LogStatus.PASS,message);
	}
	public static void ExtentFail(ExtentTest test, String scrshot) {
		test.log(LogStatus.FAIL,"Test Failed" + scrshot);
	}
	public static void LogInfo(ExtentTest test, String message) {
		test.log(LogStatus.INFO, message);
	}
	public static void LogError(ExtentTest test, String message) {
		test.log(LogStatus.ERROR, message);
	}
	public static void LogFatal(ExtentTest test, String message) {
		test.log(LogStatus.FATAL, message);
	}
	public static void LogSkip(ExtentTest test, String message) {
		test.log(LogStatus.SKIP, message);
	}
	public static void LogUnknown(ExtentTest test, String message) {
		test.log(LogStatus.UNKNOWN, message);
	}
	public static void LogWarn(ExtentTest test, String message) {
		test.log(LogStatus.WARNING, message);
	}

	public static String screenshot(WebDriver driver) {
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		File Dest = new File(PropertyFileReader.loadfile().getProperty("screenshot") + LocalDateTime.now().format(FORMATTER) + ".png");
		String destpath = Dest.getAbsolutePath();
		try {
			FileUtils.copyFile(scrFile, Dest);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return destpath;
	}

	public static String addScreenshot(ExtentTest test, WebDriver driver) {
		return test.addScreenCapture(ExtentReport.screenshot(driver));
	}
}
