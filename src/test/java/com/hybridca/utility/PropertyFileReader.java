package com.hybridca.utility;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertyFileReader {

	public static Properties loadfile() {
		String loc = ".//config.properties";
		
		Properties prop = null;
		try {
			prop = new Properties();
			prop.load(new FileInputStream(loc));
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return prop;
	}

}
