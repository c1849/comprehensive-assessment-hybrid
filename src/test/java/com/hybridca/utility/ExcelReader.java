package com.hybridca.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelReader {

	public static Sheet getExcel(String filePath ,String fileName,String sheetName) {
		File file =    new File(filePath+"\\"+fileName);

		Workbook workbook = null;
		try {
			workbook = new XSSFWorkbook(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
		} catch (IOException e) {
			e.printStackTrace();
			
		}

		return workbook.getSheet(sheetName);
	}

	public static ArrayList<String> readExcel(String testcase) {

		Sheet sheet = getExcel(PropertyFileReader.loadfile().getProperty("excelpath"),
				PropertyFileReader.loadfile().getProperty("excelfile"),
				PropertyFileReader.loadfile().getProperty("excelsheet"));

		Iterator<Row> rows = sheet.rowIterator();
		Row firstrow = rows.next();

		Iterator<Cell> cellIter = firstrow.cellIterator();

//		int rowCount = sheet.getLastRowNum() - sheet.getFirstRowNum();
//		int columnCount = firstrow.getLastCellNum() - firstrow.getFirstCellNum();

		int testcaseColumn = 0;
		while(cellIter.hasNext()) {
			Cell cell = cellIter.next();
			if(cell.getStringCellValue().equalsIgnoreCase("Testcases")) {
				testcaseColumn = cell.getColumnIndex();
			}
		}
//		Object[][] data = new Object[columnCount-1][rowCount];
		
		ArrayList<String> data = new ArrayList<String>();
		while(rows.hasNext()) {
			Row nextRow = rows.next();
			if(nextRow.getCell(testcaseColumn).getStringCellValue().equalsIgnoreCase(testcase)) {
				Iterator<Cell> cells = nextRow.cellIterator();
				Cell cell = cells.next();
				while(cells.hasNext()){
					cell = cells.next();
					data.add(cell.getStringCellValue());
//					data[cell.getColumnIndex()-1][cell.getRowIndex()-1] = cell.getStringCellValue();
				}	
			}
		}

		return data;
		
	}
}
