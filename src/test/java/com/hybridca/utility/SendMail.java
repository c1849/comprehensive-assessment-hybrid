package com.hybridca.utility;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendMail {
	 private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("MM/dd/yyyy");
	
	public static void sendMail() {
		final String from = PropertyFileReader.loadfile().getProperty("senderemail");
		final String pass = PropertyFileReader.loadfile().getProperty("password");
		String to = PropertyFileReader.loadfile().getProperty("recipientemail");

		String host = PropertyFileReader.loadfile().getProperty("emailhost");

		Properties properties = System.getProperties();

//		properties.put("mail.smtp.host", host);
//		properties.put("mail.smtp.port", "465");
//		properties.put("mail.smtp.ssl.enable", "true");
//		properties.put("mail.smtp.auth", "true");
		
		properties.put("mail.smtp.user", from);
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", "465");
        properties.put("mail.smtp.starttls.enable","true");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.debug", "true");
        properties.put("mail.smtp.socketFactory.port", "465");
        properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        properties.put("mail.smtp.socketFactory.fallback", "true");
        properties.put("mail.smtp.ssl.trust", host);


		Session session = Session.getInstance(properties, new javax.mail.Authenticator() {

			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(from, pass);
			}
		
		});

		session.setDebug(true);

		try {
			MimeMessage message = new MimeMessage(session);

			message.setFrom(new InternetAddress(from));

			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));

			message.setSubject("Project Test Report");
			
			MimeMultipart multipart = new MimeMultipart();
			
			MimeBodyPart msgBody1 = new MimeBodyPart();
			MimeBodyPart msgBody2 = new MimeBodyPart();
			
			msgBody1.setText("This is an automated email.\n\n"+"Test Report for Project of Date: " +  LocalDateTime.now().format(FORMATTER) +" is enclosed within.");
			msgBody2.attachFile(PropertyFileReader.loadfile().getProperty("report") + ExtentReport.reportName);
//			msgBody2.setDataHandler(new DataHandler(new FileDataSource(PropertyFileReader.loadfile().getProperty("report") + ExtentReport.reportName)));

			multipart.addBodyPart(msgBody1);
			multipart.addBodyPart(msgBody2);
			
			message.setContent(multipart);
//			System.out.println("sending...");

			Transport.send(message);
			System.out.println("=====Email Sent=====");
		} catch (MessagingException mex) {
			mex.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
